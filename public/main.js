var map = L.map('map');

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    maxZoom: 16
}).addTo(map);

map.on('locationfound', onLocationFound);
map.on('locationerror', onLocationError);

map.locate({ setView: true, maxZoom: 13 });

async function onLocationFound(e) {
    var userData = await getUserData();

    var dataDiv = document.getElementById("coord");
    var content = document.createTextNode(`Latitude: ${e.latitude}, Longitude: ${e.longitude}`);
    dataDiv.appendChild(content);

    var users = await sendUserData(userData.user, userData.email, e.latitude, e.longitude);
    usersToMap(users);
    console.log(map)
}

async function onLocationError() {
    var dataDiv = document.getElementById("coord");
    var content = document.createTextNode(`User denied access to location!`);

    dataDiv.appendChild(content);
    var users = await getMemory();

    usersToMap(users);
    map.setView([users.at(-1).lat, users.at(-1).lng], 10);
}

function usersToMap(users) {
    users.forEach(user => {
        L.marker([user.lat, user.lng]).addTo(map)
            .bindPopup(`${user.name}, ${user.email}, ${user.loginDate}`)
    });
}

async function getUserData() {
    const repsonse = await fetch('/userData');
    const userData = await repsonse.json()
    return userData;
}

async function getMemory() {
    const repsonse = await fetch('/memory');
    const memory = await repsonse.json()
    return memory;
}

async function sendUserData(name, email, latitude, longitude) {
    const response = await fetch('/addUser', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: name,
            email: email,
            lat: latitude,
            lng: longitude
        })
    });
    
    const users = await response.json();
    return users;
}