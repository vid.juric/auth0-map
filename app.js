const express = require('express')
const app = express();

const dotenv = require('dotenv');
dotenv.config();

let memory = [
  { name: "Marko Horvat", email: "marko@gmail.com", loginDate: "12:01:23 03.11.2021.", lat: 45.8150, lng: 15.9819 },
  { name: "Ivan Kovačević", email: "ivan@gmail.com", loginDate: "15:34:54 03.11.2021.", lat: 45.8011, lng: 15.7110 },
  { name: "Marija  Babić", email: "marija@gmail.com", loginDate: "09:37:31 04.11.2021.", lat: 45.8572, lng: 15.8047 },
  { name: "Goran Marić", email: "goran@gmail.com", loginDate: "10:55:46 04.11.2021.", lat: 45.7142, lng: 16.0752 },
  { name: "Ivana Novak", email: "ivana@gmail.com", loginDate: "11:26:22 04.11.2021.", lat: 45.8059, lng: 16.2378 }
];

const { auth, requiresAuth } = require('express-openid-connect');
const PORT = process.env.PORT || 3000

const config = {
  authRequired: false,
  idpLogout: true,
  issuerBaseURL: process.env.ISSUER_BASE_URL,
  baseURL: process.env.BASE_URL || `https://localhost:${PORT}`,
  clientID: process.env.CLIENT_ID,
  secret: process.env.SECRET,
  clientSecret: process.env.CLIENT_SECRET,
  authorizationParams: {
    response_type: 'code'
  }
};

app.use(auth(config));
app.use(express.static('public'));
app.use(express.json());
app.set('view engine', 'pug');

app.get('/', requiresAuth(), (req, res) => {
  const user = req.oidc.user.name;
  res.render('map', { user });
});

app.get('/userData', requiresAuth(), (req, res) => {
  res.send({ user: req.oidc.user.name, email: req.oidc.user.email });
});

app.get('/memory', requiresAuth(), (req, res) => {
  res.send(memory);
});

app.post('/addUser', requiresAuth(), (req, res) => {
  if (checkForDuplicates(req.body.name, req.body.email, req.body.lat, req.body.lng)) {
    if (memory.length == 5) {
      memory.shift();
    }

    memory.push({ name: req.body.name, email: req.body.email, loginDate: getCurrentDate(), lat: req.body.lat, lng: req.body.lng });
  }

  res.send(memory);
});

if (process.env.PORT) {
  app.listen(PORT, () => {
    console.log(`Listening on port: ${PORT}`)
  })

} else {

  var fs = require('fs');
  var https = require('https');

  https.createServer({
    key: fs.readFileSync('server.key'),
    cert: fs.readFileSync('server.cert')
  }, app)
    .listen(PORT, function () {
      console.log(`Server running at https://localhost:${PORT}/`);
    });
}

function getCurrentDate() {
  var currentDate = new Date();
  var date = currentDate.toLocaleDateString('hr-HR').replace(/\s+/g, '');
  var time = currentDate.toLocaleTimeString('hr-HR', {timeZone: 'Europe/Zagreb' });
  return time + ' ' + date;
}

function checkForDuplicates(user, email, lat, lng) {
  var index = memory.findIndex(x => x.name === user && x.email == email);
  if(index !== -1){
    memory[index].lat = lat;
    memory[index].lng = lng;
    memory[index].loginDate = getCurrentDate();
    return false;
  }
  return true;
}